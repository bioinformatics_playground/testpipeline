nextflow.enable.dsl=2

include { SAY_HELLO } from "./say_hello/say_hello.nf"

workflow {
    greetings_ch = Channel.of("Hello", "Ola", "Namaste")
    SAY_HELLO(greetings_ch)
}
